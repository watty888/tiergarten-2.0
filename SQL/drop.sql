drop view multipleAnimalSupervisors
/

drop view assignedAnimals
/

drop view totalAnimals
/

drop table Buy
/

drop table Sells
/

drop table Ticket
/

drop table Visitor
/

drop table Cashier
/

drop table Feeds
/

drop table Nutrition
/

drop table Animal
/

drop table AnimalSupervisor
/

drop table Cage
/

drop table DepartmentManager
/

drop sequence deptSeq
/

drop table Employee
/

drop table Department
/

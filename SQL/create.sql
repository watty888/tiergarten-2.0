CREATE TABLE Department
(
    departmentID   INT,
    departmentName VARCHAR(50) UNIQUE NOT NULL,
    numberOfCages  INT DEFAULT 0,
    CONSTRAINT pk_department PRIMARY KEY (departmentId)
)
/

CREATE TABLE Employee
(
    employeeID   INT GENERATED ALWAYS AS IDENTITY (START with 1 INCREMENT by 1),
    firstName    VARCHAR(25)                      NOT NULL,
    lastName     VARCHAR(25)                      NOT NULL,
    employeeSIN  CHAR(7) DEFAULT '0000000' UNIQUE NOT NULL,
    supervisorID INT,

    CONSTRAINT pk_employee PRIMARY KEY (employeeId),
    CONSTRAINT fk_employeeSupID FOREIGN KEY (supervisorID)
        REFERENCES Employee (employeeID)
)
/

CREATE SEQUENCE deptSeq START WITH 1
/
CREATE OR REPLACE TRIGGER deptPlus
    BEFORE INSERT
    ON Department
    FOR EACH ROW
BEGIN
    SELECT deptSeq.nextval
    INTO :NEW.departmentId
    FROM dual;
END;
/

CREATE TABLE DepartmentManager
(
    deptManagerId    INT GENERATED ALWAYS AS IDENTITY,
    deptManagerEmpId INT,
    depManDeptNo     INT,
    email            VARCHAR(30) NOT NULL,

    CONSTRAINT pk_deptManager PRIMARY KEY (deptManagerId),
    CONSTRAINT fk_deptManagerEmpId FOREIGN KEY (deptManagerEmpId)
        REFERENCES Employee (employeeId) ON DELETE CASCADE,
    CONSTRAINT fk_deptMangerDeptNo FOREIGN KEY (depManDeptNo)
        REFERENCES Department (departmentId) ON DELETE CASCADE
)
/

CREATE TABLE Cage
(
    cageId         INT GENERATED ALWAYS AS IDENTITY,
    cageDeptNo     INT,
    cageSize       INT DEFAULT 30 CHECK (cageSize >= 30) NOT NULL,
    animalCapacity INT DEFAULT 0,

    CONSTRAINT pk_cage PRIMARY KEY (cageId),
    CONSTRAINT fk_cageDeptNo FOREIGN KEY (cageDeptNo)
        REFERENCES Department (departmentId) ON DELETE CASCADE
)
/

CREATE TABLE AnimalSupervisor
(
    animalSupervisorId    INT GENERATED ALWAYS AS IDENTITY,
    anSupEmpId            INT         NOT NULL,
    speciesSpecialization VARCHAR(10) NOT NULL,
    noOfAssignedAnimals   INT DEFAULT 0,
    anSupSupervisorID     INT,

    CONSTRAINT pk_animalSupervisor PRIMARY KEY (animalSupervisorId),
    CONSTRAINT fk_anSupEmpId FOREIGN KEY (anSupEmpId)
        REFERENCES Employee (employeeId) ON DELETE CASCADE
)
/

CREATE TABLE Animal
(
    animalId           INT GENERATED ALWAYS AS IDENTITY,
    animalSupervisorId INT,
    animalCageNo       INT         NOT NULL,
    animalSpecies      VARCHAR(30) NOT NULL,
    name               VARCHAR(25) NOT NULL,
    age                INT         NOT NULL,

    CONSTRAINT pk_animal PRIMARY KEY (animalId),
    CONSTRAINT fk_animalSupervisorId FOREIGN KEY (animalSupervisorId)
        REFERENCES AnimalSupervisor (animalSupervisorId) ON DELETE CASCADE,
    CONSTRAINT fk_animalCageNo FOREIGN KEY (animalCageNo)
        REFERENCES Cage (cageId) ON DELETE CASCADE
)
/

CREATE TABLE Nutrition
(
    nutritionId INT GENERATED ALWAYS AS IDENTITY,
    type        VARCHAR(40) UNIQUE NOT NULL,

    CONSTRAINT pk_nutrition PRIMARY KEY (nutritionId)
)
/

CREATE TABLE Feeds
(
    animalSupId INT,
    nutritionId INT,
    animalId    INT,

    CONSTRAINT fk_feedsAnimalSupId FOREIGN KEY (animalSupId)
        REFERENCES AnimalSupervisor (animalSupervisorId) ON DELETE CASCADE,
    CONSTRAINT fk_feedsNutritionId FOREIGN KEY (nutritionId)
        REFERENCES Nutrition (nutritionId) ON DELETE CASCADE,
    CONSTRAINT fk_feedsAnimalId FOREIGN KEY (animalId)
        REFERENCES Animal (animalId) ON DELETE CASCADE
)
/

CREATE TABLE Cashier
(
    cashierId    INT GENERATED ALWAYS AS IDENTITY,
    cashierEmpId INT,
    cashNo       INT            NOT NULL,
    fax          CHAR(8) UNIQUE NOT NULL,

    CONSTRAINT pk_cashier PRIMARY KEY (cashierId),
    CONSTRAINT fk_cashierEmpId FOREIGN KEY (cashierEmpId)
        REFERENCES Employee (employeeId) ON DELETE CASCADE
)
/

CREATE TABLE Visitor
(
    visitorID       INT GENERATED ALWAYS AS IDENTITY,
    visitorSIN      CHAR(8) UNIQUE,
    firstName       VARCHAR(50) NOT NULL,
    lastName        VARCHAR(50) NOT NULL,
    methodOfPayment VARCHAR(15) DEFAULT 'Cash',

    CONSTRAINT pk_visitor PRIMARY KEY (visitorID)
)
/

CREATE TABLE Ticket
(
    category VARCHAR(10),
    price    NUMBER(*, 2) CHECK (price >= 0) NOT NULL,

    CONSTRAINT pk_ticketType PRIMARY KEY (category)
)
/

CREATE TABLE Sells
(
    sellID         INT GENERATED ALWAYS AS IDENTITY,
    cashierId      INT         NOT NULL,
    ticketCategory VARCHAR(10) NOT NULL,
    visitorID      INt         NOT NULL,

    CONSTRAINT pk_sells PRIMARY KEY (sellID),
    CONSTRAINT fk_sellsCashierId FOREIGN KEY (cashierId)
        REFERENCES Cashier (cashierId) ON DELETE CASCADE,
    CONSTRAINT fk_sellsTicketId FOREIGN KEY (ticketCategory)
        REFERENCES Ticket (category) ON DELETE CASCADE,
    CONSTRAINT fk_sellsVisitorID FOREIGN KEY (visitorID)
        REFERENCES Visitor (visitorID) ON DELETE CASCADE
)
/

CREATE TABLE Buy
(
    buyID          INT GENERATED ALWAYS AS IDENTITY,
    visitorID      INT         NOT NULL,
    ticketCategory VARCHAR(10) NOT NULL,

    CONSTRAINT pk_buy PRIMARY KEY (buyID),
    CONSTRAINT fk_buyVisitorID FOREIGN KEY (visitorID)
        REFERENCES Visitor (visitorID) ON DELETE CASCADE,
    CONSTRAINT fk_buyTicketId FOREIGN KEY (ticketCategory)
        REFERENCES Ticket (category) ON DELETE CASCADE
)
/

--  Count all animals
CREATE VIEW totalAnimals(Animals) AS
SELECT COUNT(animalId)
FROM Animal
/

-- calculate number of animals that is assigned to supervisor
CREATE VIEW assignedAnimals (EmployeeName, animalID) AS
SELECT Employee.firstName, Animal.animalId
FROM AnimalSupervisor
         LEFT JOIN Animal ON AnimalSupervisor.animalSupervisorId = Animal.animalId
         LEFT JOIN Employee ON AnimalSupervisor.AnimalSupervisorId = Employee.employeeId
/

-- calculate the number of animal supervisors who supervise more than one animal
CREATE VIEW multipleAnimalSupervisors (supervisorID, numberOfAnimals) AS
SELECT Animal.animalSupervisorId, COUNT(Animal.animalId)
FROM Animal
GROUP BY Animal.animalId, Animal.animalSupervisorId
HAVING COUNT(animalId) > 1
/


CREATE OR REPLACE PROCEDURE p_update_feeding_plan(p_animal_id IN FEEDS.ANIMALID%TYPE,
                                                  p_nutrition_id IN FEEDS.NUTRITIONID%TYPE,
                                                  p_error_code OUT NUMBER)
AS
BEGIN
    UPDATE FEEDS SET NUTRITIONID = p_nutrition_id WHERE ANIMALID = p_animal_id;
    p_error_code := SQL%ROWCOUNT;
    IF (p_error_code = 1)
    THEN
        COMMIT;
    ELSE
        ROLLBACK;
    END IF;
EXCEPTION
    WHEN OTHERS
        THEN
            p_error_code := SQLCODE;
END p_update_feeding_plan;

CREATE OR REPLACE PROCEDURE p_delete_animal(p_animal_id IN ANIMAL.ANIMALID%TYPE,
                                            p_error_code OUT NUMBER)
AS
BEGIN
    DELETE
    FROM ANIMAL
    WHERE p_animal_id = ANIMAL.ANIMALID;

    p_error_code := SQL%ROWCOUNT;
    IF (p_error_code = 1)
    THEN
        COMMIT;
    ELSE
        ROLLBACK;
    END IF;
EXCEPTION
    WHEN OTHERS
        THEN
            p_error_code := SQLCODE;
END p_delete_animal;

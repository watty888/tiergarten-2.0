INSERT INTO DEPARTMENT (departmentName, numberOfCages) VALUES ('Savanna', '5');
INSERT INTO DEPARTMENT (departmentName, numberOfCages) VALUES ('Forest', '5');
INSERT INTO DEPARTMENT (departmentName, numberOfCages) VALUES ('Steppe', '5');
INSERT INTO DEPARTMENT (departmentName, numberOfCages) VALUES ('Desert', '5');
INSERT INTO DEPARTMENT (departmentName, numberOfCages) VALUES ('Tundra', '5');


INSERT INTO EMPLOYEE (firstName, lastName, employeeSIN)
  VALUES ('Barry', 'White', '1230988');
INSERT INTO DEPARTMENTMANAGER (deptManagerEmpId, depManDeptNo, email)
  VALUES (1, 1, 'barry@white.dept');
INSERT INTO EMPLOYEE (firstName, lastName, employeeSIN)
  VALUES ('B.B.', 'King', '1231239');
INSERT INTO DEPARTMENTMANAGER (deptManagerEmpId, depManDeptNo, email)
  VALUES (2, 2, 'bb@king.dept');
INSERT INTO EMPLOYEE (firstName, lastName, employeeSIN)
  VALUES ('George', 'Benson', '9234230');
INSERT INTO DEPARTMENTMANAGER (deptManagerEmpId, depManDeptNo, email)
  VALUES (3, 3, 'george@benson.dept');
INSERT INTO EMPLOYEE (firstName, lastName, employeeSIN)
  VALUES ('Ray', 'Charles', '4321900');
INSERT INTO DEPARTMENTMANAGER (deptManagerEmpId, depManDeptNo, email)
  VALUES (4, 4, 'ray@charles.dept');
INSERT INTO EMPLOYEE (firstName, lastName, employeeSIN)
  VALUES ('Louis', 'Armstrong', '2304988');
INSERT INTO DEPARTMENTMANAGER (deptManagerEmpId, depManDeptNo, email)
  VALUES (5, 5, 'louis@armstrong.dept');

INSERT INTO CAGE (cageDeptNo, cageSize, noOfAnimals) VALUES (1, 30, 1);
INSERT INTO CAGE (cageDeptNo, cageSize, noOfAnimals) VALUES (1, 30, 1);
INSERT INTO CAGE (cageDeptNo, cageSize, noOfAnimals) VALUES (1, 30, 1);
INSERT INTO CAGE (cageDeptNo, cageSize, noOfAnimals) VALUES (1, 30, 1);
INSERT INTO CAGE (cageDeptNo, cageSize, noOfAnimals) VALUES (1, 30, 1);
INSERT INTO CAGE (cageDeptNo, cageSize, noOfAnimals) VALUES (2, 30, 1);
INSERT INTO CAGE (cageDeptNo, cageSize, noOfAnimals) VALUES (2, 30, 1);
INSERT INTO CAGE (cageDeptNo, cageSize, noOfAnimals) VALUES (2, 30, 1);
INSERT INTO CAGE (cageDeptNo, cageSize, noOfAnimals) VALUES (2, 30, 1);
INSERT INTO CAGE (cageDeptNo, cageSize, noOfAnimals) VALUES (2, 30, 1);
INSERT INTO CAGE (cageDeptNo, cageSize, noOfAnimals) VALUES (3, 30, 1);
INSERT INTO CAGE (cageDeptNo, cageSize, noOfAnimals) VALUES (3, 30, 1);
INSERT INTO CAGE (cageDeptNo, cageSize, noOfAnimals) VALUES (3, 30, 1);
INSERT INTO CAGE (cageDeptNo, cageSize, noOfAnimals) VALUES (3, 30, 1);
INSERT INTO CAGE (cageDeptNo, cageSize, noOfAnimals) VALUES (3, 30, 1);
INSERT INTO CAGE (cageDeptNo, cageSize, noOfAnimals) VALUES (4, 30, 1);
INSERT INTO CAGE (cageDeptNo, cageSize, noOfAnimals) VALUES (4, 30, 1);
INSERT INTO CAGE (cageDeptNo, cageSize, noOfAnimals) VALUES (4, 30, 1);
INSERT INTO CAGE (cageDeptNo, cageSize, noOfAnimals) VALUES (4, 30, 1);
INSERT INTO CAGE (cageDeptNo, cageSize, noOfAnimals) VALUES (4, 30, 1);
INSERT INTO CAGE (cageDeptNo, cageSize, noOfAnimals) VALUES (5, 30, 1);
INSERT INTO CAGE (cageDeptNo, cageSize, noOfAnimals) VALUES (5, 30, 1);
INSERT INTO CAGE (cageDeptNo, cageSize, noOfAnimals) VALUES (5, 30, 1);
INSERT INTO CAGE (cageDeptNo, cageSize, noOfAnimals) VALUES (5, 30, 1);
INSERT INTO CAGE (cageDeptNo, cageSize, noOfAnimals) VALUES (5, 30, 1);

INSERT INTO EMPLOYEE (firstName, lastName, employeeSIN)
  VALUES ('Stevie', 'Wonder', '4823948');
INSERT INTO ANIMALSUPERVISOR (anSupEmpId, specialization, noOfAssignedAnimals)
  VALUES (6, 'Lions', 1);
INSERT INTO EMPLOYEE (firstName, lastName, employeeSIN)
  VALUES ('Jimmy', 'Hendrix', '481948');
INSERT INTO ANIMALSUPERVISOR (anSupEmpId, specialization, noOfAssignedAnimals)
  VALUES (7, 'Tigers', 1);
INSERT INTO EMPLOYEE (firstName, lastName, employeeSIN)
  VALUES ('Nat King', 'Cole', '3424211');
INSERT INTO ANIMALSUPERVISOR (anSupEmpId, specialization, noOfAssignedAnimals)
  VALUES (8, 'Foxes', 1);
INSERT INTO EMPLOYEE (firstName, lastName, employeeSIN)
  VALUES ('Quincy', 'Jones', '9910232');
INSERT INTO ANIMALSUPERVISOR (anSupEmpId, specialization, noOfAssignedAnimals)
  VALUES (9, 'Elephants', 1);
INSERT INTO EMPLOYEE (firstName, lastName, employeeSIN)
  VALUES ('Frank', 'Sinatra', '9123880');
INSERT INTO ANIMALSUPERVISOR (anSupEmpId, specialization, noOfAssignedAnimals)
  VALUES (10, 'Zebras', 1);

INSERT INTO ANIMAL (animalSupervisorId, animalCageNo, name, species, age)
  VALUES (1,  1, 'boo', 'lion', 4);
INSERT INTO ANIMAL (animalSupervisorId, animalCageNo, name, species, age)
  VALUES (1,  1, 'foo', 'lion', 4);
INSERT INTO ANIMAL (animalSupervisorId, animalCageNo, name, species, age)
  VALUES (1,  1, 'elly', 'lion', 4);
INSERT INTO ANIMAL (animalSupervisorId, animalCageNo, name, species, age)
  VALUES (1,  1, 'johny', 'lion', 4);
INSERT INTO ANIMAL (animalSupervisorId, animalCageNo, name, species, age)
  VALUES (1,  1, 'alex', 'lion', 4);
INSERT INTO ANIMAL (animalSupervisorId, animalCageNo, name, species, age)
  VALUES (2,  2, 'gretta', 'tiger', 4);
INSERT INTO ANIMAL (animalSupervisorId, animalCageNo, name, species, age)
  VALUES (2,  2, 'hanna', 'tiger', 4);
INSERT INTO ANIMAL (animalSupervisorId, animalCageNo, name, species, age)
  VALUES (2,  2, 'bobby', 'tiger', 4);
INSERT INTO ANIMAL (animalSupervisorId, animalCageNo, name, species, age)
  VALUES (2,  2, 'sammy', 'tiger', 4);
INSERT INTO ANIMAL (animalSupervisorId, animalCageNo, name, species, age)
  VALUES (2,  2, 'lisy', 'tiger', 4);
INSERT INTO ANIMAL (animalSupervisorId, animalCageNo, name, species, age)
  VALUES (3,  3, 'joeffry', 'fox', 4);
INSERT INTO ANIMAL (animalSupervisorId, animalCageNo, name, species, age)
  VALUES (3,  3, 'ramsey', 'fox', 4);
INSERT INTO ANIMAL (animalSupervisorId, animalCageNo, name, species, age)
  VALUES (3,  3, 'sersei', 'fox', 4);
INSERT INTO ANIMAL (animalSupervisorId, animalCageNo, name, species, age)
  VALUES (3,  3, 'kitty', 'fox', 4);
INSERT INTO ANIMAL (animalSupervisorId, animalCageNo, name, species, age)
  VALUES (3,  3, 'betty', 'fox', 4);
INSERT INTO ANIMAL (animalSupervisorId, animalCageNo, name, species, age)
  VALUES (4,  4, 'kim', 'elephant', 4);
INSERT INTO ANIMAL (animalSupervisorId, animalCageNo, name, species, age)
  VALUES (4,  4, 'stan', 'elephant', 4);
INSERT INTO ANIMAL (animalSupervisorId, animalCageNo, name, species, age)
  VALUES (4,  4, 'ivan', 'elephant', 4);
INSERT INTO ANIMAL (animalSupervisorId, animalCageNo, name, species, age)
  VALUES (4,  4, 'sergei', 'elephant', 4);
INSERT INTO ANIMAL (animalSupervisorId, animalCageNo, name, species, age)
  VALUES (4,  4, 'pete', 'elephant', 4);
INSERT INTO ANIMAL (animalSupervisorId, animalCageNo, name, species, age)
  VALUES (5,  5, 'kate', 'zebra', 4);
INSERT INTO ANIMAL (animalSupervisorId, animalCageNo, name, species, age)
  VALUES (5,  5, 'mat', 'zebra', 4);
INSERT INTO ANIMAL (animalSupervisorId, animalCageNo, name, species, age)
  VALUES (5,  5, 'ann', 'zebra', 4);
INSERT INTO ANIMAL (animalSupervisorId, animalCageNo, name, species, age)
  VALUES (5,  5, 'ahmed', 'zebra', 4);
INSERT INTO ANIMAL (animalSupervisorId, animalCageNo, name, species, age)
  VALUES (5,  5, 'gustav', 'zebra', 4);

INSERT INTO NUTRITION (name, typeOfAnimal, portionSize) VALUES ('meat', 'lion', 5);
INSERT INTO NUTRITION (name, typeOfAnimal, portionSize) VALUES ('grass', 'tiger', 5);
INSERT INTO NUTRITION (name, typeOfAnimal, portionSize) VALUES ('mouse', 'fox', 5);
INSERT INTO NUTRITION (name, typeOfAnimal, portionSize) VALUES ('seeds', 'elephant', 5);
INSERT INTO NUTRITION (name, typeOfAnimal, portionSize) VALUES ('hay', 'zebra 1', 5);

INSERT INTO FEEDS (animalSupId, nutritionId, animalId) VALUES (1, 1, 1);
INSERT INTO FEEDS (animalSupId, nutritionId, animalId) VALUES (1, 1, 2);
INSERT INTO FEEDS (animalSupId, nutritionId, animalId) VALUES (1, 1, 3);
INSERT INTO FEEDS (animalSupId, nutritionId, animalId) VALUES (1, 1, 4);
INSERT INTO FEEDS (animalSupId, nutritionId, animalId) VALUES (1, 1, 5);
INSERT INTO FEEDS (animalSupId, nutritionId, animalId) VALUES (2, 2, 6);
INSERT INTO FEEDS (animalSupId, nutritionId, animalId) VALUES (2, 2, 7);
INSERT INTO FEEDS (animalSupId, nutritionId, animalId) VALUES (2, 2, 8);
INSERT INTO FEEDS (animalSupId, nutritionId, animalId) VALUES (2, 2, 9);
INSERT INTO FEEDS (animalSupId, nutritionId, animalId) VALUES (2, 2, 10);
INSERT INTO FEEDS (animalSupId, nutritionId, animalId) VALUES (3, 3, 11);
INSERT INTO FEEDS (animalSupId, nutritionId, animalId) VALUES (3, 3, 12);
INSERT INTO FEEDS (animalSupId, nutritionId, animalId) VALUES (3, 3, 13);
INSERT INTO FEEDS (animalSupId, nutritionId, animalId) VALUES (3, 3, 14);
INSERT INTO FEEDS (animalSupId, nutritionId, animalId) VALUES (3, 3, 15);
INSERT INTO FEEDS (animalSupId, nutritionId, animalId) VALUES (4, 4, 16);
INSERT INTO FEEDS (animalSupId, nutritionId, animalId) VALUES (4, 4, 17);
INSERT INTO FEEDS (animalSupId, nutritionId, animalId) VALUES (4, 4, 18);
INSERT INTO FEEDS (animalSupId, nutritionId, animalId) VALUES (4, 4, 19);
INSERT INTO FEEDS (animalSupId, nutritionId, animalId) VALUES (4, 4, 20);
INSERT INTO FEEDS (animalSupId, nutritionId, animalId) VALUES (5, 5, 21);
INSERT INTO FEEDS (animalSupId, nutritionId, animalId) VALUES (5, 5, 22);
INSERT INTO FEEDS (animalSupId, nutritionId, animalId) VALUES (5, 5, 23);
INSERT INTO FEEDS (animalSupId, nutritionId, animalId) VALUES (5, 5, 24);
INSERT INTO FEEDS (animalSupId, nutritionId, animalId) VALUES (5, 5, 25);

INSERT INTO EMPLOYEE (firstName, lastName, employeeSIN)
  VALUES ('James', 'Brown', '2321333');
INSERT INTO CASHIER (cashierEmpId, cashNo, fax)
  VALUES (11, 1, 1251313);
INSERT INTO EMPLOYEE (firstName, lastName, employeeSIN)
  VALUES ('Miles', 'Davis', '00123234');
INSERT INTO CASHIER (cashierEmpId, cashNo, fax)
  VALUES (12, 2, 1431313);
INSERT INTO EMPLOYEE (firstName, lastName, employeeSIN)
  VALUES ('Stevie', 'Wonder', '2134643');
INSERT INTO CASHIER (cashierEmpId, cashNo, fax)
  VALUES (13, 3, 1231383);

INSERT INTO VISITOR (firstName, lastName, methodOfPayment) VALUES ('visitor1', 'name1', 'cash');
INSERT INTO VISITOR (firstName, lastName, methodOfPayment) VALUES ('visitor2', 'name2', 'cc');
INSERT INTO VISITOR (firstName, lastName, methodOfPayment) VALUES ('visitor3', 'name3', 'cash');
INSERT INTO VISITOR (firstName, lastName, methodOfPayment) VALUES ('visitor4', 'name4', 'cc');
INSERT INTO VISITOR (firstName, lastName, methodOfPayment) VALUES ('visitor5', 'name5', 'cash');


INSERT INTO TICKET (price, visSIN) VALUES (15, 1);
INSERT INTO TICKET (price, visSIN) VALUES (15, 2);
INSERT INTO TICKET (price, visSIN) VALUES (15, 3);
INSERT INTO TICKET (price, visSIN) VALUES (15, 4);
INSERT INTO TICKET (price, visSIN) VALUES (15, 4);

INSERT INTO SELLS (cashierId, ticketId) VALUES (1, 1);
INSERT INTO SELLS (cashierId, ticketId) VALUES (1, 2);
INSERT INTO SELLS (cashierId, ticketId) VALUES (1, 3);
INSERT INTO SELLS (cashierId, ticketId) VALUES (2, 4);
INSERT INTO SELLS (cashierId, ticketId) VALUES (2, 5);

INSERT INTO BUY (visitorSIN, tickedId) VALUES (1, 1);
INSERT INTO BUY (visitorSIN, tickedId) VALUES (2, 2);
INSERT INTO BUY (visitorSIN, tickedId) VALUES (3, 3);
INSERT INTO BUY (visitorSIN, tickedId) VALUES (4, 4);
INSERT INTO BUY (visitorSIN, tickedId) VALUES (5, 5);

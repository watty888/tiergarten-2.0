
//Database Systems (Module IDS)

import java.sql.*;
import java.util.ArrayList;

// The DatabaseHelper class encapsulates the communication with our database
class DatabaseHelper {
    RandomHelper rdm = new RandomHelper();
    // Database connection info
    private static final String DB_CONNECTION_URL = "jdbc:oracle:thin:@oracle-lab.cs.univie.ac.at:1521:lab";
    private static final String USER = "a01449747";
    private static final String PASS = "z,kjrj(88)";
    // The name of the class loaded from the ojdbc14.jar driver file
    private static final String CLASSNAME = "oracle.jdbc.driver.OracleDriver";

    // We need only one Connection and one Statement during the execution => class
    // variable
    private static Statement stmt;
    private static Connection con;

    private void insertIntoEmployee(String fn, String ln, String sin) {
        try {
            // * first insert employee into table
            String sqlEmpl = "INSERT INTO Employee (firstName, lastName, employeeSIN) VALUES ('" + fn + "', '" + ln
                    + "', '" + sin + "')";
            stmt.execute(sqlEmpl);
        } catch (Exception e) {
            System.err.println("Error at: insertIntoEmployee3\nmessage: " + e.getMessage());
        }
    }

    private void insertIntoEmployee(String fn, String ln, String sin, int supID) {
        try {
            // * first insert employee into table
            String sqlEmpl = "INSERT INTO Employee (firstName, lastName, employeeSIN, supervisorID) VALUES ('" + fn
                    + "', '" + ln + "', '" + sin + "', '" + supID + "')";
            stmt.execute(sqlEmpl);
        } catch (Exception e) {
            System.err.println("Error at: insertIntoEmployee4\nmessage: " + e.getMessage());
        }
    }

    private int getLatestEmployeeId() {
        try {
            ResultSet rs = stmt.executeQuery("SELECT MAX(employeeID) FROM Employee");

            rs.next();

            return rs.getInt(1);
        } catch (Exception e) {
            System.err.println("Error at: getLatestEmployee\nmessage: " + e.getMessage());
            return -1;
        }

    }

    // CREATE CONNECTION
    DatabaseHelper() {
        try {
            Class.forName(CLASSNAME);
            String database = DB_CONNECTION_URL;
            String user = USER;
            String pass = PASS;

            // establish connection to database
            con = DriverManager.getConnection(database, user, pass);
            stmt = con.createStatement();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void insertIntoDepartment(String deptName, int noOfCages) {
        try {
            String sql = "INSERT INTO Department (departmentName, numberOfCages) VALUES ('" + deptName + "', '"
                    + noOfCages + "')";
            stmt.execute(sql);
        } catch (Exception e) {
            System.err.println("Error at: insertIntoDept\nmessage: " + e.getMessage());
        }
    }

    void insertIntoDepartmentManager(String firstName, String lastName, String sid, int depId, String email) {
        try {
            // * first insert employee into table
            this.insertIntoEmployee(firstName, lastName, sid);

            // * then retrieve its id to add to the dependant table
            Integer latestAddedEmployee = this.getLatestEmployeeId();
            String sqlDM = "INSERT INTO DepartmentManager (deptManagerEmpId, depManDeptNo, email) VALUES ('"
                    + latestAddedEmployee + "', '" + depId + "', '" + email + "')";
            stmt.execute(sqlDM);
        } catch (Exception e) {
            System.err.println("Error at: insertIntoDept\nmessage: " + e.getMessage());
        }
    }

    void insertIntoCage(int deptId) {
        try {
            // * Get number of cages from each department
            ResultSet rs = stmt
                    .executeQuery("SELECT numberOfCages FROM Department WHERE departmentID = '" + deptId + "'");
            rs.next();
            Integer numberOfCages = rs.getInt(1);

            for (int i = 0; i < numberOfCages; ++i) {
                int cageSize = rdm.getRandomInteger(30, 100);
                int animalCapacity = rdm.getRandomInteger(1, 5);
                String sql1 = "INSERT INTO Cage (cageDeptNo, cageSize, animalCapacity)  VALUES ('" + deptId + "', '"
                        + cageSize + "', '" + animalCapacity + "')";

                stmt.execute(sql1);
            }
        } catch (Exception e) {
            System.err.println("Error at: insertIntoCage\nmessage: " + e.getMessage());
        }
    }

    void insertIntoAnimalSupervisor(String firstName, String lastName, String sin, int deptID) {
        try {
            // * first insert employee into table
            this.insertIntoEmployee(firstName, lastName, sin, deptID);

            // * then retrieve its id to add to the dependant table
            Integer latestAddedEmployee = this.getLatestEmployeeId();

            // * Retrieve department name and set it as speciesSpecialization
            ResultSet rs = stmt.executeQuery("SELECT departmentName FROM Department WHERE departmentID = " + deptID);
            rs.next();

            String specName = rs.getString(1);
            int supID = deptID;

            String sqlAS = "INSERT INTO AnimalSupervisor (anSupEmpId, speciesSpecialization, anSupSupervisorID)"
                    + "VALUES (" + latestAddedEmployee + ", '" + specName + "', " + supID + ")";

            stmt.execute(sqlAS);
        } catch (Exception e) {
            System.err.println("Error at: insertIntoAnimalSupervisor\nmessage: " + e.getMessage());
        }
    }

    void insertIntoAnimal(int cid, int did, String spec, String name, int age) {
        String sql = "INSERT INTO Animal (animalSupervisorId, animalCageNo, animalSpecies, name, age)" + " VALUES ("
                + did + ", " + cid + ", '" + spec + "', '" + name + "', " + age + ")";

        String sqlUpdate = "UPDATE AnimalSupervisor SET noOfAssignedAnimals = noOfAssignedAnimals + 1 WHERE animalSupervisorId = '"
                + did + "'";

        try {
            stmt.execute(sql);
            stmt.execute(sqlUpdate);
        } catch (Exception e) {
            System.err.println("Error at: insertIntoAnimal\nmessage: " + e.getMessage());
        }
    }

    void insertIntoNutrition(String type) {
        String sql = "INSERT INTO Nutrition (type)" + " VALUES ('" + type + "')";

        try {
            stmt.execute(sql);
        } catch (Exception e) {
            System.err.println("Error at: insertIntoNutrition\nmessage: " + e.getMessage());
        }
    }

    void insertIntoFeeds(int ansSupID, int nutritionID, int animalID) {
        String sql = "INSERT INTO Feeds (animalSupId, nutritionId, animalId)" + " VALUES (" + ansSupID + ", "
                + nutritionID + ", " + animalID + ")";

        try {
            stmt.execute(sql);
        } catch (Exception e) {
            System.err.println("Error at: insertIntoNutrition\nmessage: " + e.getMessage());
        }
    }

    void insertIntoCashier(String firstName, String lastName, String sin, int cashNo, String fax) {
        // * first insert employee into table
        this.insertIntoEmployee(firstName, lastName, sin);

        // * then retrieve its id to add to the dependant table
        Integer latestAddedEmployee = this.getLatestEmployeeId();

        String sql = "INSERT INTO Cashier (cashierEmpId, cashNo, fax)" + " VALUES (" + latestAddedEmployee + ", "
                + cashNo + ", " + fax + ")";

        try {
            stmt.execute(sql);
        } catch (Exception e) {
            System.err.println("Error at: insertIntoNutrition\nmessage: " + e.getMessage());
        }
    }

    void insertIntoVisitor(String sin, String firstName, String lastName, String methodOfPayment) {
        String sql = "INSERT INTO Visitor (visitorSIN, firstName, lastName, methodOfPayment) VALUES ('" + sin + "', '"
                + firstName + "', '" + lastName + "', '" + methodOfPayment + "')";

        try {
            stmt.execute(sql);
        } catch (Exception e) {
            System.err.println("Error at: insertIntoVisitor\nmessage: " + e.getMessage());
        }
    }

    void insertTicket(String category, double price) {
        String sql = "INSERT INTO Ticket (category, price) VALUES ('"
                + category + "', " + price + ")";

        try {
            stmt.execute(sql);
        } catch (Exception e) {
            System.err.println("Error at: insertTicket\nmessage: " + e.getMessage());
        }
    }

    void insertIntoSells(int cashierID, String category, int visitorID) {
        String sql = "INSERT INTO Sells (cashierId, ticketCategory, visitorID) VALUES (" + cashierID + ", '" + category + "'," + visitorID + ")";

        try {
            stmt.execute(sql);
        } catch (Exception e) {
            System.err.println("Error at: insertIntoSells\nmessage: " + e.getMessage());
        }
    }

    void insertIntoBuy(int id) {
        String sql = "INSERT INTO Buy (visitorID, ticketID) VALUES (" + id + ", " + id + ")";

        try {
            stmt.execute(sql);
        } catch (Exception e) {
            System.err.println("Error at: insertIntoBuy\nmessage: " + e.getMessage());
        }
    }

    ArrayList<Integer> selectDepartmentIdsFromDepartment() {
        ArrayList<Integer> IDs = new ArrayList<>();

        try {
            ResultSet rs = stmt.executeQuery("SELECT departmentID FROM Department ORDER BY departmentID");
            while (rs.next()) {
                IDs.add(rs.getInt("departmentID"));
            }
            rs.close();
        } catch (Exception e) {
            System.err.println(("Error at: selectDepartmentIdsFromDepartment\n message: " + e.getMessage()).trim());
        }
        return IDs;
    }

    public void close() {
        try {
            stmt.close(); // clean up
            con.close();
        } catch (Exception ignored) {
        }
    }

    // UTILITY FUNCTIONS
    int getCageCapacity(String option) {
        String query = "";

        switch (option) {
        case "TOTAL":
            query = "SELECT SUM(animalCapacity) FROM Cage";
            break;
        default:
            query = "SELECT animalCapacity FROM Cage WHERE cageId = " + option;
        }

        try {
            ResultSet rs = stmt.executeQuery(query);

            rs.next();

            return rs.getInt(1);
        } catch (Exception e) {
            System.err.println("Error at: getTotalAnimals\nmessage: " + e.getMessage());
            return -1;
        }
    }

    int getCagesCount(String option) {
        String query = "";

        switch (option) {
        case "ALL":
            query = "SELECT COUNT(*) FROM Cage";
            break;
        default:
            query = "SELECT numberOfCages FROM Department WHERE departmentID = " + option;
        }

        try {
            ResultSet rs = stmt.executeQuery(query);

            rs.next();

            return rs.getInt(1);
        } catch (Exception e) {
            System.err.println("Error at: getTotalAnimals\nmessage: " + e.getMessage());
            return -1;
        }
    }

    int getTotalCages() {
        String query = "SELECT COUNT(*) FROM Cage";

        try {
            ResultSet rs = stmt.executeQuery(query);

            rs.next();

            return rs.getInt(1);
        } catch (Exception e) {
            System.err.println("Error at: getTotalCages\nmessage: " + e.getMessage());
            return -1;
        }
    }

    boolean isCageInDept(int cid, int did) {
        String query = "SELECT cageId FROM Cage WHERE cageId = "
                + cid + " AND cageDeptNo = " + did;

        try {
            ResultSet rs = stmt.executeQuery(query);

            if (rs.next()) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            System.err.println("Error at: isCageInDept\nmessage: " + e.getMessage());
            return false;
        }
    }

    boolean isAnimalSupervisedBy(int asid, int aid) {
        String query = "SELECT animalId FROM Animal WHERE animalId = " + aid + " AND animalSupervisorId = " + asid;

        try {
            ResultSet rs = stmt.executeQuery(query);

            if (rs.next()) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            System.err.println("Error at: isAnimalSupervisedBy\nmessage: " + e.getMessage());
            return false;
        }
    }

    void showNumberOfDataSets(String tableName) {
        try {
            ResultSet re = stmt.executeQuery("SELECT COUNT(*) FROM " + tableName);
            if (re.next()) {
                int count = re.getInt(1);
                System.out.println("Number of datasets in " + tableName + ": " + count);
            }

        } catch (Exception e) {
            System.err.println("Error at: showNumberOfDataSets\nmessage: " + e.getMessage());
        }
    }

}

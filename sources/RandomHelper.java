//Database Systems (Module IDS)

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

// The RandomHelper class wraps around the JAVA Random class to provide convenient access to random data as we need it
// Additionally it provides access to external single-columned files (e.g. courses.csv, names.csv, surnames.csv)
class RandomHelper {
  private final char[] symbols = getDigits();
  private Random rand;
  private ArrayList<String> firstNames;
  private ArrayList<String> lastNames;
  private ArrayList<String> emails;
  private ArrayList<String> animals;
  private ArrayList<String> mops;
  private static final String firstNameFile = "../dummyData/names.csv";
  private static final String lastNameFile = "../dummyData/surnames.csv";
  private static final String emailsFile = "../dummyData/emails.csv";
  private static final String animalsFile = "../dummyData/animalNames.csv";
  private static final String methodsOfPayment = "../dummyData/paymentOptions.csv";

  // instantiate the Random object and store data from files in lists
  RandomHelper() {
    this.rand = new Random();
    this.lastNames = readFile(lastNameFile);
    this.firstNames = readFile(firstNameFile);
    this.emails = readFile(emailsFile);
    this.animals = readFile(animalsFile);
    this.mops = readFile(methodsOfPayment);
  }

  String getRandomSIN() {
    StringBuilder out = new StringBuilder();
    final int sidLen = 7;
    // int len = rand.nextInt((maxLen - minLen) + 1) + minLen;
    while (out.length() < sidLen) {
      int idx = Math.abs((rand.nextInt() % symbols.length));
      out.append(symbols[idx]);
    }
    return out.toString();
  }

  String getRandomFAX() {
    StringBuilder out = new StringBuilder();
    final int faxLen = 8;
    // int len = rand.nextInt((maxLen - minLen) + 1) + minLen;
    while (out.length() < faxLen) {
      int idx = Math.abs((rand.nextInt() % symbols.length));
      out.append(symbols[idx]);
    }
    return out.toString();
  }

  // returns random element from list
  String getRandomFirstName() {
    return firstNames.get(getRandomInteger(0, firstNames.size() - 1));
  }

  // returns random element from list
  String getRandomLastName() {
    return lastNames.get(getRandomInteger(0, lastNames.size() - 1));
  }

  // returns random element from list
  String getRandomAnimalName() {
    return animals.get(getRandomInteger(0, animals.size() - 1));
  }

  // returns random element from list
  String getRandomEmail() {
    return emails.get(getRandomInteger(0, emails.size() - 1));
  }

  // returns random double from the Interval [min, max] and a defined precision
  // (e.g. precision:2 => 3.14)
  Double getRandomDouble(double min, double max, int precision) {
    // Hack that is not the cleanest way to ensure a specific precision, but...
    double r = Math.pow(10, precision);
    return Math.round(min + (rand.nextDouble() * (max - min)) * r) / r;
  }

  // return random Integer from the Interval [min, max]; (min, max are possible as
  // well)
  Integer getRandomInteger(int min, int max) {
    return rand.nextInt((max - min) + 1) + min;
  }

  String getRandomMOP() {
    return mops.get(getRandomInteger(0, mops.size() - 1));
  }

  String getRandomSpecies(List<String> species) {
    int idx = new Random().nextInt(species.size());

    return species.get(idx);
  }

  // reads single-column files and stores its values as Strings in an ArraList
  private ArrayList<String> readFile(String filename) {
    String line;
    ArrayList<String> set = new ArrayList<>();
    try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
      while ((line = br.readLine()) != null) {
        try {
          set.add(line);
        } catch (Exception ignored) {
        }
      }

    } catch (Exception e) {
      e.printStackTrace();
    }
    return set;
  }

  // defines which chars are used to create random strings
  private char[] getDigits() { // create getCharSet char array
    StringBuffer b = new StringBuffer(128);
    for (int i = 48; i <= 57; i++)
      b.append((char) i); // 0-9
    return b.toString().toCharArray();
  }
}

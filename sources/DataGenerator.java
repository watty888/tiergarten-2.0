
//Database Systems (Module IDS)

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DataGenerator {
    public static void main(String args[]) {

        RandomHelper rdm = new RandomHelper();
        DatabaseHelper dbHelper = new DatabaseHelper();

        // * 1) Insert Into Department
        String[] deptName = { "Savanna", "Steppe", "Forest", "Desert", "Tundra" };
        Integer[] noOfCages = { 5, 5, 5, 5, 5 };

        for (int i = 0; i < 5; i++) {
            dbHelper.insertIntoDepartment(deptName[i], noOfCages[i]);
        }

        dbHelper.showNumberOfDataSets("Department");

        // * 2) Assign Department Managers to each Department
        for (int i = 0; i < deptName.length; ++i) {
            String firstName = rdm.getRandomFirstName();
            String lastName = rdm.getRandomLastName();
            String sin = rdm.getRandomSIN();
            String email = rdm.getRandomEmail();
            int depID = i + 1;

            dbHelper.insertIntoDepartmentManager(firstName, lastName, sin, depID, email);
        }

        dbHelper.showNumberOfDataSets("DepartmentManager");

        // * 3) Add cages based on number of departments and number of cages
        for (int deptID = 1; deptID <= deptName.length; ++deptID) {
            dbHelper.insertIntoCage(deptID);
        }

        dbHelper.showNumberOfDataSets("Cage");

        // * 4) Insert into animal Supervisor
        List<List<String>> species = new ArrayList<>();
        List<String> savanna = new ArrayList<>(Arrays.asList("Lion", "Giraffe", "Zebra", "Antilope", "Hyppo"));
        List<String> steppe = new ArrayList<>(Arrays.asList("Leopard", "Puma", "Bison", "Dingo", "Fox"));
        List<String> forest = new ArrayList<>(Arrays.asList("Greasly", "Wild Deer", "Elk", "Wild Boar", "Panda"));
        List<String> desert = new ArrayList<>(
                Arrays.asList("Spotted Hyena", "Ostrich", "Rattlesnake", "Desrt Toad", "Greater Roadrunner"));
        List<String> tundra = new ArrayList<>(
                Arrays.asList("Arctic Fox", "Musk Ox", "Snowy Owl", "Polar Bear", "Caribou"));

        species.add(savanna);
        species.add(steppe);
        species.add(forest);
        species.add(desert);
        species.add(tundra);

        for (int deptId = 1; deptId <= species.size(); ++deptId) {
            String firstName = rdm.getRandomFirstName();
            String lastName = rdm.getRandomLastName();
            String sin = rdm.getRandomSIN();
            dbHelper.insertIntoAnimalSupervisor(firstName, lastName, sin, deptId);
        }

        dbHelper.showNumberOfDataSets("AnimalSupervisor");

        // * 5) Insert into animal
        final int totalCapacity = dbHelper.getCageCapacity("TOTAL");
        int currentCageID = 1;
        int currentCageDeptID = 1;
        int currentCageCapacity = dbHelper.getCageCapacity(Integer.toString(currentCageID));
        int capacityChecker = 0;
        String spec = rdm.getRandomSpecies(species.get(currentCageDeptID - 1));

        for (int animal = 1; animal <= totalCapacity; ++animal) {
            String name = rdm.getRandomAnimalName();
            int age = rdm.getRandomInteger(1, 10);

            if (capacityChecker == currentCageCapacity) {
                capacityChecker = 0;
                ++currentCageID;
                currentCageCapacity = dbHelper.getCageCapacity(Integer.toString(currentCageID));

                if (!species.get(currentCageDeptID - 1).isEmpty()) {
                    spec = rdm.getRandomSpecies(species.get(currentCageDeptID - 1));

                    species.get(currentCageDeptID - 1).remove(spec);
                }
            }

            if (!dbHelper.isCageInDept(currentCageID, currentCageDeptID)) {
                ++currentCageDeptID;
            }

            dbHelper.insertIntoAnimal(currentCageID, currentCageDeptID, spec, name, age);
            ++capacityChecker;
        }

        dbHelper.showNumberOfDataSets("Animal");

        // * 6) Insert into Nutrition
        List<String> nutrtionType = new ArrayList<>(Arrays.asList("TYPE-A", "TYPE-B", "TYPE-C"));

        dbHelper.insertIntoNutrition(nutrtionType.get(0));
        dbHelper.insertIntoNutrition(nutrtionType.get(1));
        dbHelper.insertIntoNutrition(nutrtionType.get(2));

        dbHelper.showNumberOfDataSets("Nutrition");

        // * 7) Insert into FEEDS
        int currentAnimalSup = 1;

        for (int animal = 1; animal <= totalCapacity; ++animal) {
            if (!dbHelper.isAnimalSupervisedBy(currentAnimalSup, animal)) {
                ++currentAnimalSup;
            }

            final int nutritionType = rdm.getRandomInteger(1, 3);

            dbHelper.insertIntoFeeds(currentAnimalSup, nutritionType, animal);
        }

        dbHelper.showNumberOfDataSets("Feeds");

        // * 8) Insert into Cashier
        for (int i = 1; i <= 5; ++i) {
            String firstName = rdm.getRandomFirstName();
            String lastName = rdm.getRandomLastName();
            String sin = rdm.getRandomSIN();
            String fax = rdm.getRandomFAX();

            dbHelper.insertIntoCashier(firstName, lastName, sin, i, fax);
        }

        dbHelper.showNumberOfDataSets("Cashier");

        // * 9) Insert into Visitor
        for (int i = 0; i < 1000; ++i) {
            String sin = rdm.getRandomSIN();
            String firstName = rdm.getRandomFirstName();
            String lastName = rdm.getRandomLastName();
            String mop = rdm.getRandomMOP();

            dbHelper.insertIntoVisitor(sin, firstName, lastName, mop);
        }

        dbHelper.showNumberOfDataSets("Visitor");

        // * 10 Insert into ticket
        String[] ticketType = { "Standard", "Family", "Group", "Senior" };
        double[] ticketPrice = { 12.40, 19.50, 35, 10.40 };

        for (int i = 0; i < ticketType.length; ++i) {
            dbHelper.insertTicket(ticketType[i], ticketPrice[i]);
        }

        dbHelper.showNumberOfDataSets("Ticket");

        // * 11 Inert into Sells
        for (int visitorID = 1; visitorID <= 1000; ++visitorID) {
            int cashierID = rdm.getRandomInteger(1, 5);
            int idx = rdm.getRandomInteger(0, 3);
            String category = ticketType[idx];

            dbHelper.insertIntoSells(cashierID, category, visitorID);
        }

        dbHelper.showNumberOfDataSets("Sells");

        dbHelper.close();
    }
}
